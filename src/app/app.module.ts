/**
 * O Angular é um Framework Modular
 * O app.module.ts é o Módulo Padrão (DEFAULT) que Gerencia os nossos Componentes
 */

import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutes } from './app.routes';

import { AppComponent } from './app.component';
import { BreadcrumbService } from './layout/breadcrumb/breadcrumb.service';
import { MenuService } from './layout/menu/app.menu.service';
import { CoreModule } from './core/core.module';
import { SegurancaModule } from './view/seguranca/seguranca.module';
import { JwtHttpInterceptor } from './auth/jwt-http-interceptor';
import { ConfirmDialogModule } from 'primeng';

@NgModule({
    imports: [
        AppRoutes,

        CoreModule,
        SegurancaModule,
        ConfirmDialogModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtHttpInterceptor, multi: true}, BreadcrumbService, MenuService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
