import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppMainComponent } from './layout/main/app.main.component';
import { AppNotfoundComponent } from './core/pages/notfound/app.notfound.component';
import { AppAccessdeniedComponent } from './core/pages/accessdenied/app.accessdenied.component';
import { AppErrorComponent } from './core/pages/error/app.error.component';

export const routes: Routes = [
    // Rotas Funcionalidades e Configuração de Carregamento Tardio (Lazy Loading)
    { path: '', component: AppMainComponent,
        children: [
            { path: '', loadChildren: './view/dashboard/dashboard.module#DashboardModule' },
            { path: 'usuarios', loadChildren: './view/usuarios/usuario.module#UsuarioModule' },
            { path: 'perfis', loadChildren: './view/perfis/perfil.module#PerfilModule' },
            { path: 'categorias', loadChildren: './view/categorias/categoria.module#CategoriaModule' },
            { path: 'ingredientes', loadChildren: './view/ingredientes/ingrediente.module#IngredienteModule' },
            { path: 'unidadesmedida', loadChildren: './view/unidadesmedida/unidademedida.module#UnidadeMedidaModule' },
            { path: 'produtos', loadChildren: './view/produtos/produto.module#ProdutoModule' },
            { path: 'empresa', loadChildren: './view/empresas/empresa.module#EmpresaModule' }
        ]
    },
    // Rotas Genéricas
    { path: 'error', component: AppErrorComponent },
    { path: 'notfound', component: AppNotfoundComponent },
    { path: 'accessdenied', component: AppAccessdeniedComponent },
    { path: '**', redirectTo: 'notfound' }

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled', useHash: true}, );
