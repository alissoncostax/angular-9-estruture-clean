/**
 * Classe JS de Serviço que Realiza o Login
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {

  static PERFIL_EMPRESA = 1;
  static PERFIL_USUARIO = 2;
  static PERFIL_ADMIN = 99;

  oauthTokenUrl: string;
  // Atributo do PayLoad do Token Gerado
  jwtPayLoad: any;

  // Construtor Montado toda vez que a Tela é Reiniciada
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {
    this.oauthTokenUrl = `${environment.apiUrl}/oauth/token`;
    this.carregarToken();
  }

  login(usuario: string, senha: string): Promise<void> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/x-www-form-urlencoded')
      // Esse Authorization é a Autorização da API e não do Usuário!
      .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    const body = `username=${usuario}&password=${senha}&grant_type=password`;

    return this.http.post<any>(this.oauthTokenUrl, body, { headers, withCredentials: true }).toPromise().then(response => {
        /*
          Vai ser Passado todo o Conteúdo do Acess_Token para
          ser Decodificado para ter Acesso ao PayLoad
        */
        this.armazenarToken(response.access_token);
      }).catch(response => {
        if (response.status === 400) {
          if (response.error === 'invalid_grant') {
            return Promise.reject('Usuário ou Senha Inválida!');
          }
        }
        return Promise.reject(response);
      });

  }

  obterNovoAccessToken(): Promise<void> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/x-www-form-urlencoded')
      // Esse Authorization é a Autorização da API e não do Usuário!
      .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    const body = 'grant_type=refresh_token';

    return this.http.post<any>(this.oauthTokenUrl, body, { headers, withCredentials: true }).toPromise().then(response => {
        this.armazenarToken(response.access_token);

        console.log('Novo Access Token Criado!');

        return Promise.resolve(null);
      })
      .catch(response => {
        console.error('Erro ao Renovar Token.', response);
        return Promise.resolve(null);
      });

  }

  /**
   * Remove o Token do LocalStorage e
   * Limpa o PayLoad, onde fica armazenado os Dados do
   * Header da Resposta da Requisição.
   */
  limparAcessToken() {
    localStorage.removeItem('token');
    this.jwtPayLoad = null;
  }

  /**
   * Verifica se o Token Expirou
   */
  isAccessTokenInvalido() {
    const token = localStorage.getItem('token');
    return !token || this.jwtHelper.isTokenExpired(token);
  }

  /**
   * Verificar a Permissão de Usuário
   */
  temPermissao(permissao: string) {
    return this.jwtPayLoad && this.jwtPayLoad.authorities.includes(permissao);
  }

  /**
   * Recebe o Array de ROLES passado pelo auth.guard e verifica
   * se o Usuário tem alguma permissão para acessar determinada
   * funcionalidade do Sistema.
   */
  verificarPermissoes(routingData) {
    // Verifica se o Perfil tem Acesso
    if (routingData.profile.includes(this.jwtPayLoad.perfil)) {
      // Verifica se Tem Permissao
      for (const role of routingData.roles) {
        if (this.temPermissao(role)) {
          return true;
        }
      }
    }
    return false;
  }

  private armazenarToken(token: string) {
    this.jwtPayLoad = this.jwtHelper.decodeToken(token);
    /**
     * É Preciso armazenar o Token no Local Storage,
     * Pois o Usuário pode Atualizar o Navegador e Pretendemos
     * Continuar com o Login Autenticado.
     */
    localStorage.setItem('token', token);
  }

  /**
   * Método é Chamado toda vez que a Tela é Atualizada,
   * pois, ao realizar o Login é guardado o Token no Local Storage e
   * ao recarregar a Página é montado um novo Objeto e o Construtor
   * é responsável por Verificar se existe o Token e Adiciona-lo
   * novamente ao Local Storage
   */
  private carregarToken() {
    const token = localStorage.getItem('token');
    if (token) {
      this.armazenarToken(token);
    }
  }

}
