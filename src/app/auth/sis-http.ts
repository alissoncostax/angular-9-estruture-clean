/**
 * Classe que Herda do AuthHttp, onde é delegado todos os Métodos
 * POST, GET, DELETE e etc. Ou seja,
 * para cada tipo de Requisição verifica se o Token está Válido!
 * Caso o Token esteja Inválido, será criado um Novo Access Token
 * de Forma Automática.
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';

import { Observable, from as observableFromPromise } from 'rxjs';

import { AuthService } from './auth.service';

export class NotAuthenticatedError {

}

@Injectable()
export class SisHttp extends HttpClient {

  // Tudo o que é declarado no Construtor é utilizado na Classe
  constructor(
    private auth: AuthService,
    private httpHandler: HttpHandler
  ) {
    // Chama os Construtor da Classe HttpHandler
    super(httpHandler);
  }

  public delete<T>(url: string, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.delete<T>(url, options));
  }

  public patch<T>(url: string, body: any, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.patch<T>(url, options));
  }

  public head<T>(url: string, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.head<T>(url, options));
  }

  public options<T>(url: string, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.options<T>(url, options));
  }

  public get<T>(url: string, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.get<T>(url, options));
  }

  public post<T>(url: string, body: any, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.post<T>(url, body, options));
  }

  public put<T>(url: string, body: any, options?: any): Observable<T> {
    return this.fazerRequisicao<T>(() => super.put<T>(url, body, options));
  }

  // Retorna o Resultado da Requisição
  private fazerRequisicao<T>(fn: Function): Observable<T> {
    // Verifica se o Token é Válido/Existe.
    if (this.auth.isAccessTokenInvalido()) {
      console.log('Requisição HTTP com Access Token Inválido. Obtendo Novo Token...');
      // Caso não exista ou esteja expirado, é feito a chamada do novo Access Token
      const chamadaNovoAccessToken = this.auth.obterNovoAccessToken()
        .then(() => {
          /**
           * Caso o Token e o Refresh Token estejam Inválidos,
           * Dispara a Exceção!
           */
          if (this.auth.isAccessTokenInvalido) {
            throw new NotAuthenticatedError();
          }
          return fn().toPromise();
        });

      return observableFromPromise(chamadaNovoAccessToken);
    } else {
      // Caso o Token já exista e seja Válido, retorna o resultado da Função
      return fn();
    }
  }

}
