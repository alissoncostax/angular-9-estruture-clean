// Módulo que Deixa os Services Disponivel Para Toda Aplicação
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMainComponent } from '../layout/main/app.main.component';
import { AppMenuComponent } from '../layout/menu/app.menu.component';
import { AppMenuitemComponent } from '../layout/menu/app.menuitem.component';
import { AppConfigComponent } from '../layout/config/app.config.component';
import { AppBreadcrumbComponent } from '../layout/breadcrumb/app.breadcrumb.component';
import { PanelModule, TabViewModule, InputTextModule, MessageService, ConfirmationService, ConfirmDialogModule } from 'primeng';
import { ErrorHandlerService } from './error-handler.service';
import { SharedModule } from '../shared/shared.module';
import { AppTopBarComponent } from '../layout/topbar/app.topbar.component';
import { AppFooterComponent } from '../layout/footer/app.footer.component';
import { AuthService } from '../auth/auth.service';
import { SisHttp } from '../auth/sis-http';

import localePt from '@angular/common/locales/pt';
import { AppErrorComponent } from './pages/error/app.error.component';
import { AppNotfoundComponent } from './pages/notfound/app.notfound.component';
import { AppAccessdeniedComponent } from './pages/accessdenied/app.accessdenied.component';
import { PerfilService } from '../view/perfis/perfil.service';
import { PermissaoService } from '../view/perfis/permissao.service';
import { ToastrModule } from 'ngx-toastr';
import { CategoriaService } from '../view/categorias/categoria.service';
import { IngredienteService } from '../view/ingredientes/ingrediente.service';
import { UnidadeMedidaService } from '../view/unidadesmedida/unidademedida.service';
import { ProdutoService } from '../view/produtos/produto.service';
import { EmpresaService } from '../view/empresas/empresa.service';
import { PlanoService } from '../view/empresas/plano.service';
import { UsuarioService } from '../view/usuarios/usuario.service';

// Define o LocalePt para toda a Aplicação!
registerLocaleData(localePt);

@NgModule({
  imports: [
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,

    // Módulo de Utilização para o MENU
    TabViewModule,
    PanelModule,
    InputTextModule,
    ConfirmDialogModule,

    // Módulo de Configuração das Rotas
    RouterModule,

    ToastrModule.forRoot()
  ],
  declarations: [
    AppErrorComponent,
    AppNotfoundComponent,
    AppAccessdeniedComponent,

    AppMainComponent,
    AppMenuComponent,
    AppMenuitemComponent,
    AppConfigComponent,
    AppBreadcrumbComponent,
    AppTopBarComponent,
    AppFooterComponent
  ],
  exports: [
    AppMainComponent,
    AppMenuComponent,
    AppMenuitemComponent,
    AppConfigComponent,
    AppBreadcrumbComponent,
    AppTopBarComponent,
    AppFooterComponent
  ],
  providers: [
    // Declaração dos Services que serão usados na Aplicação
    AuthService,
    ErrorHandlerService,
    SisHttp,

    MessageService,
    ConfirmationService,

    UsuarioService,
    PerfilService,
    PermissaoService,
    CategoriaService,
    IngredienteService,
    UnidadeMedidaService,
    ProdutoService,
    EmpresaService,
    PlanoService,
    {provide: LOCALE_ID, useValue: 'pt'}
  ]
})
export class CoreModule { }
