import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http'
import { NotAuthenticatedError } from '../auth/sis-http';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorHandlerService {

  // Construtor para Redirecionar para outras Páginas
  constructor(private toastr: ToastrService, private router: Router) { }

  handle(errorResponse: any) {
    let msg: string;

    if (typeof errorResponse === 'string') {
      msg = errorResponse;
    } else if (errorResponse instanceof NotAuthenticatedError) {
      // Caso o Access Token e o Refresh Token Estejam Expirados.
      msg = 'Sua Sessão está Expirada! Faça Login Novamente.'
      this.router.navigate(['/login']);
    } else if (errorResponse instanceof HttpErrorResponse
      && errorResponse.status >= 400 && errorResponse.status <= 499) {

      msg = 'Ocorreu um Erro ao Processar a sua Solicitação.';

      if (errorResponse.status === 403) {
        msg = 'Você não tem Permissão para Executar essa Ação!'
      }

      if (errorResponse.status === 400 && errorResponse.error.error === 'invalid_grant') {
        msg = errorResponse.error.error_description;
      }
      try {
        msg = errorResponse.error[0].mensagemUsuario;
      } catch (e) { }
        console.error('Ocorreu um Erro!', errorResponse);
    } else {
      msg = 'Erro ao Processor Serviço Remoto, Tente Novamente.';
      console.log('Ocorreu um Erro: ', errorResponse);
    }
    this.toastr.error(msg);
  }

}
