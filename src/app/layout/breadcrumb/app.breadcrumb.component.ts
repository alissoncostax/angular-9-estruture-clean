import { Component, OnDestroy } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';
import { Subscription } from 'rxjs';
import { MenuItem } from 'primeng/primeng';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { LogoutService } from 'src/app/view/seguranca/logout.service';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './app.breadcrumb.component.html'
})
export class AppBreadcrumbComponent implements OnDestroy {

    subscription: Subscription;

    items: MenuItem[];

    constructor(
        public breadcrumbService: BreadcrumbService, 
        public auth: AuthService,
        private logoutService: LogoutService, 
        private errorHandle: ErrorHandlerService,
        private router: Router) {
        this.subscription = breadcrumbService.itemsHandler.subscribe(response => {
            this.items = response;
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    logout() {
        this.logoutService.logout().then(() => {
            this.router.navigate(['/login']);
        }).catch(erro => this.errorHandle.handle(erro));
    }

}
