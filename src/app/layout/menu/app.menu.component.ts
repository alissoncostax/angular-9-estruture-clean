import {Component, OnInit} from '@angular/core';
import {AppMainComponent} from '../main/app.main.component';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(public app: AppMainComponent, public auth: AuthService) {}

    ngOnInit() {
        this.model = [
            {label: 'Dashboard', icon: 'dashboard', routerLink: ['/'], permissao: true},
            {
                label: 'Categorias', icon: 'extension', routerLink: ['/categorias'], 
                permissao: 
                (   
                    this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                    || this.auth.temPermissao('ROLE_PESQUISAR_CATEGORIA')
                )
            },
            {
                label: 'Ingredientes', icon: 'brush', routerLink: ['/ingredientes'], 
                permissao: 
                (   
                    this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                    || this.auth.temPermissao('ROLE_PESQUISAR_INGREDIENTE')
                )
            },
            {
                label: 'Unidades de Medida', icon: 'timeline', routerLink: ['/unidadesmedida'], 
                permissao: 
                (   
                    this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                    || this.auth.temPermissao('ROLE_PESQUISAR_UNIDADE_MEDIDA')
                )
            },
            {
                label: 'Produtos', icon: 'redeem', routerLink: ['/produtos'], 
                permissao: 
                (   
                    this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                    || this.auth.temPermissao('ROLE_PESQUISAR_PRODUTO')
                )
            },
            {
                label: 'Configurações', 
                icon: 'settings', 
                permissao: 
                (   
                    this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                ),
                items: [
                    {
                        label: 'Acesso', 
                        icon: 'laptop', 
                        permissao: 
                        (
                            this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                        ),
                        items: [
                            {
                                label: 'Usuários', icon: 'person', routerLink: ['/usuarios'], 
                                permissao: this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN 
                            },
                            {
                                label: 'Perfis', icon: 'group', routerLink: ['/perfis'], 
                                permissao: this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                            }
                        ]
                    },
                    {
                        label: 'Minha Empresa', icon: 'store', routerLink: ['/empresa'], 
                        permissao: this.auth.jwtPayLoad.perfil === AuthService.PERFIL_ADMIN
                        || this.auth.temPermissao('ROLE_ALTERAR_EMPRESA')
                    },
                ]
            },
        ];
    }

    onMenuClick() {
        this.app.menuClick = true;
    }
}
