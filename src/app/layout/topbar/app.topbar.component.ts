import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { AppMainComponent } from '../main/app.main.component';
import { AuthService } from 'src/app/auth/auth.service';
import { LogoutService } from 'src/app/view/seguranca/logout.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {

    /**
     * Anotação auth para Verificar se o Usuário tem Acesso
     * à determinada Funcionalidade presente no MENU de Acesso
     * e pegar o Nome do Usuário Logado
     */
    constructor(
        public app: AppMainComponent, 
        public auth: AuthService, 
        private logoutService: LogoutService, 
        private errorHandle: ErrorHandlerService,
        private router: Router) 
    {}

    logout() {
        this.logoutService.logout().then(() => {
            this.router.navigate(['/login']);
        }).catch(erro => this.errorHandle.handle(erro));
    }

}
