export class Produto {
  codigo: number;
  descricao: string;
  imagem: string;
  urlImagem: string;
}
