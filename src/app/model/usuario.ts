import { Perfil } from './perfil';

export class Usuario {
  codigo: number;
  nome: string;
  login: string;
  senha: string;
  perfil = new Perfil();
}
