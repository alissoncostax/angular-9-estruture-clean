import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoriaCadastroComponent } from './categorias-cadastro/categoria-cadastro.component';
import { CategoriaPesquisaComponent } from './categorias-pesquisa/categoria-pesquisa.component';
import { CategoriaRoutingModule } from './categoria-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule, TooltipModule, ButtonModule, InputTextModule, DropdownModule, SpinnerModule } from 'primeng';

@NgModule({
  imports: [
    SharedModule,
    CategoriaRoutingModule,
    ReactiveFormsModule,

    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    SpinnerModule

  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    CategoriaCadastroComponent,
    CategoriaPesquisaComponent
  ],
  exports: []
})
export class CategoriaModule { }
