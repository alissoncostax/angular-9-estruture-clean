import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SisHttp } from 'src/app/auth/sis-http';
import { Categoria } from 'src/app/model/categoria';

@Injectable()
export class CategoriaService {

  categoriaUrl: string;

  constructor(private http: SisHttp) {
    this.categoriaUrl = `${environment.apiUrl}/categorias`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(empresa: number): Promise<any> {
    return this.http.get<any>(`${this.categoriaUrl}/empresa/${empresa}`).toPromise();
  }

  listarCategoriasCardapio(): Promise<any> {
    return this.http.get(`${this.categoriaUrl}?itens`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<any> {
    return this.http.get(`${this.categoriaUrl}/excluir/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(categoria: Categoria): Promise<Categoria> {
    return this.http.post<Categoria>(this.categoriaUrl, categoria).toPromise();
  }

  // Método de Atualizar
  atualizar(categoria: Categoria): Promise<Categoria> {
    console.log('tag', categoria);
    return this.http.put<Categoria>(`${this.categoriaUrl}/${categoria.codigo}`, categoria).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.categoriaUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  // Carregar a Combo com Todos os Perfis ATIVOS
  listarPerfis(): Promise<any> {
    // Filtrar por Status Ativo
    const params = new HttpParams().append('status', 'true');
    return this.http.get(`${this.categoriaUrl}`, { params }).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Categoria> {
    return this.http.get<Categoria>(`${this.categoriaUrl}/${codigo}`).toPromise();
  }

}
