import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { CategoriaService } from './../categoria.service';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-categoria-cadastro',
  templateUrl: './categoria-cadastro.component.html'
})
export class CategoriaCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private auth: AuthService,
    private categoriaService: CategoriaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoCategoria = history.state.categoria;
    if (codigoCategoria) {
      this.carregarCategoria(codigoCategoria);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoCategoria) {
      this.router.navigate(['/error']);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      numIdentificador: ['', [ this.validarObrigatoriedade ]],
      descricao: [ '', [ this.validarObrigatoriedade ] ],
      empresa: this.auth.jwtPayLoad.codigoEmpresa
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarCategoria(codigo: number) {
    this.categoriaService.buscarPorCodigo(codigo).then(categoria => {
      this.formulario.patchValue(categoria);
    }).catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarCategoria();
    } else {
      this.adicionarCategoria();
    }
  }

  adicionarCategoria() {
    this.categoriaService.adicionar(this.formulario.value).then(categoriaAdicionado => {
      this.toastr.success('Categoria Adicionada com Sucesso!');
      this.router.navigate(['/categorias']);
    }).catch(erro => 
      console.log('tag', erro));
  }

  atualizarCategoria() {
    this.categoriaService.atualizar(this.formulario.value).then(categoria => {
      this.formulario.patchValue(categoria);
      this.toastr.success('Categoria Atualizada com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
