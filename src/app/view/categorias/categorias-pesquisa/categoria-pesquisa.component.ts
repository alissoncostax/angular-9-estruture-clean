import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CategoriaService } from './../categoria.service';
import { Categoria } from 'src/app/model/categoria';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationService } from 'primeng';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria-pesquisa',
  templateUrl: './categoria-pesquisa.component.html'
})
export class CategoriaPesquisaComponent implements OnInit {

  categorias: Categoria[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private categoriaService: CategoriaService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.categoriaService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(resultado => {
      this.categorias = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'numIdentificador', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'statusCategoria', header: 'Status' }
    ];
  }

  alterar(categoria) {
    this.router.navigate(['/categorias/alterar'], { state: { categoria: categoria.codigo } });
  }

  excluir(categoria: any) {
    this.categoriaService.excluir(categoria.codigo).then(resultado => {
      if (resultado) {
        this.pesquisar();
        this.toastr.success('Categoria Excluída com Sucesso!');
      } else {
        this.toastr.error('Categoria não Excluída! Existem Produtos associados à essa Categoria.');
      }
    }).catch(erro => this.errorHandler.handle(erro));
  }

  confirmarExcluir(categoria: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Categoria?',
      accept: () => {
        this.excluir(categoria);
      }
    });
  }

  alterarStatus(categoria: any): void {
    const novoStatus = !categoria.statusCategoria;

    this.categoriaService.alterarStatus(categoria.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativada' : 'Desativada';
      categoria.statusCategoria = novoStatus;
      this.toastr.success(`Categoria ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
