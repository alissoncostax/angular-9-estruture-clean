import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    DashboardRoutingModule
  ],
  declarations: [
    DashboardComponent
  ],
  exports: []
})
export class DashboardModule { }
