import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from 'src/app/auth/auth.guard';
import { EmpresaAdminComponent } from './empresas-admin/empresa-admin.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Cadastro
    path: '',
    component: EmpresaAdminComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_EMPRESA'] }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class EmpresaRoutingModule { }
