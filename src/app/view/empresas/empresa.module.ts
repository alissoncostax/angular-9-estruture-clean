import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EmpresaRoutingModule } from './empresa-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule, TooltipModule, ButtonModule, InputTextModule, DropdownModule, SpinnerModule, PasswordModule } from 'primeng';
import { EmpresaAdminComponent } from './empresas-admin/empresa-admin.component';

@NgModule({
  imports: [
    SharedModule,
    EmpresaRoutingModule,
    ReactiveFormsModule,

    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    SpinnerModule,
    DropdownModule,
    PasswordModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    EmpresaAdminComponent
  ],
  exports: []
})
export class EmpresaModule { }
