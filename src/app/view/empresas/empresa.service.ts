import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SisHttp } from 'src/app/auth/sis-http';
import { Empresa } from 'src/app/model/empresa';

@Injectable()
export class EmpresaService {

  empresaUrl: string;

  constructor(private http: SisHttp) {
    this.empresaUrl = `${environment.apiUrl}/empresas`;
  }

  // Método de Atualizar
  atualizar(empresa: Empresa): Promise<Empresa> {
    return this.http.put<Empresa>(`${this.empresaUrl}/${empresa.codigo}`, empresa).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Empresa> {
    return this.http.get<Empresa>(`${this.empresaUrl}/${codigo}`).toPromise();
  }

}
