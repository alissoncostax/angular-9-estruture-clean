import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { EmpresaService } from '../empresa.service';
import { PlanoService } from '../plano.service';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { AuthService } from 'src/app/auth/auth.service';
import { SelectItem } from 'primeng';

@Component({
  selector: 'app-empresa-admin',
  templateUrl: './empresa-admin.component.html'
})
export class EmpresaAdminComponent implements OnInit {

  formulario: FormGroup;

  planos: SelectItem[];
  status: SelectItem[];

  constructor(
    private auth: AuthService,
    private empresaService: EmpresaService,
    private planoService: PlanoService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    this.listarPlanos();

    this.listarStatus();

    const codigoEmpresa = this.auth.jwtPayLoad.codigoEmpresa;
    if (codigoEmpresa) {
      this.carregarEmpresa(codigoEmpresa);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      razaoSocial: ['', [ this.validarObrigatoriedade ]],
      nomeFantasia: ['', [ this.validarObrigatoriedade ]],
      statusDelivery: ['', [ this.validarObrigatoriedade ]],
      whatsapp: ['', [ this.validarObrigatoriedade ]],
      telefoneContato: ['', [ this.validarObrigatoriedade ]],
      plano: this.formBuilder.group({
        codigo: [null, [ this.validarObrigatoriedade ]]
      }),
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  listarPlanos() {
    this.planoService.listarPlanos().then(plano => {
        const planosBanco = plano;
        this.planos = planosBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
        this.planos.splice(0, 0, {label: 'Selecione um Plano', value: null});
    }).catch(erro => this.errorHandler.handle(erro));
  }

  listarStatus() {
    this.status = [
      {label: 'Selecione o Status do Delivery', value: null},
      {label: 'Ativo', value: true},
      {label: 'Inativo', value: false}
    ];
  }

  carregarEmpresa(codigo: number) {
    this.empresaService.buscarPorCodigo(codigo)
      .then(empresa => {
        this.formulario.patchValue(empresa);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarEmpresa();
    }
  }

  atualizarEmpresa() {
    this.empresaService.atualizar(this.formulario.value)
      .then(empresa => {
        this.formulario.patchValue(empresa);
        this.toastr.success('Empresa Atualizada com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
