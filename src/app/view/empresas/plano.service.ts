import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SisHttp } from 'src/app/auth/sis-http';

@Injectable()
export class PlanoService {

  planoUrl: string;

  constructor(private http: SisHttp) {
    this.planoUrl = `${environment.apiUrl}/planos`;
  }

  listarPlanos(): Promise<any> {
    return this.http.get(`${this.planoUrl}`, { }).toPromise();
  }

}
