import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { IngredienteCadastroComponent } from './ingredientes-cadastro/ingrediente-cadastro.component';
import { IngredientePesquisaComponent } from './ingredientes-pesquisa/ingrediente-pesquisa.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: IngredientePesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_INGREDIENTE'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: IngredienteCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_INGREDIENTE'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: IngredienteCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_INGREDIENTE'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class IngredienteRoutingModule { }
