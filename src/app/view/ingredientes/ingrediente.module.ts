import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectButtonModule } from 'primeng/selectbutton';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { IngredienteRoutingModule } from './ingrediente-routing.module';

import { IngredientePesquisaComponent } from './ingredientes-pesquisa/ingrediente-pesquisa.component';
import { IngredienteCadastroComponent } from './ingredientes-cadastro/ingrediente-cadastro.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    IngredienteRoutingModule,
    ReactiveFormsModule,
    
    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    IngredienteCadastroComponent,
    IngredientePesquisaComponent
  ],
  exports: []
})
export class IngredienteModule { }
