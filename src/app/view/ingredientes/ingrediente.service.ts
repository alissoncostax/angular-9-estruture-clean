import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SisHttp } from 'src/app/auth/sis-http';
import { environment } from 'src/environments/environment';
import { Ingrediente } from 'src/app/model/ingrediente';

@Injectable()
export class IngredienteService {

  ingredienteUrl: string;

  constructor(private http: SisHttp) {
    this.ingredienteUrl = `${environment.apiUrl}/ingredientes`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(empresa: number): Promise<any> {
    return this.http.get<any>(`${this.ingredienteUrl}/empresa/${empresa}`).toPromise();
  }

  listarIngredientes(empresa: number): Promise<any> {
    return this.pesquisar(empresa).then(response => {
        const ingredienteBanco = response;
        const resultado = {
          ingrediente: ingredienteBanco
        }
        return resultado;
      })
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.ingredienteUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(ingrediente: Ingrediente): Promise<Ingrediente> {
    return this.http.post<Ingrediente>(this.ingredienteUrl, ingrediente).toPromise();
  }

  // Método de Atualizar
  atualizar(ingrediente: Ingrediente): Promise<Ingrediente> {
    return this.http.put<Ingrediente>(`${this.ingredienteUrl}/${ingrediente.codigo}`, ingrediente).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.ingredienteUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  // Carregar a Combo com Todos os Perfis ATIVOS
  listarPerfis(): Promise<any> {
    // Filtrar por Status Ativo
    const params = new HttpParams().append('status', 'true');
    return this.http.get(`${this.ingredienteUrl}`, { params }).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Ingrediente> {
    return this.http.get<Ingrediente>(`${this.ingredienteUrl}/${codigo}`, { }).toPromise();
  }

}
