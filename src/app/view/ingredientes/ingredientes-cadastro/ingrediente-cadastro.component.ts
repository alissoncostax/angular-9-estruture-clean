import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IngredienteService } from './../ingrediente.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Ingrediente } from 'src/app/model/ingrediente';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-ingrediente-cadastro',
  templateUrl: './ingrediente-cadastro.component.html',
})
export class IngredienteCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private auth: AuthService,
    private ingredienteService: IngredienteService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoIngrediente = history.state.ingrediente;
    if (codigoIngrediente) {
      this.carregarIngrediente(codigoIngrediente);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoIngrediente) {
      this.router.navigate(['/error']);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade ] ],
      empresa: [this.auth.jwtPayLoad.codigoEmpresa]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarIngrediente(codigo: number) {
    this.ingredienteService.buscarPorCodigo(codigo)
      .then(ingrediente => {
        this.formulario.patchValue(ingrediente);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarIngrediente();
    } else {
      this.adicionarIngrediente();
    }
  }

  adicionarIngrediente() {
    this.ingredienteService.adicionar(this.formulario.value)
      .then(ingredienteAdicionado => {
        this.toastr.success('Ingrediente Adicionado com Sucesso!');
        this.router.navigate(['/ingredientes']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarIngrediente() {
    this.ingredienteService.atualizar(this.formulario.value)
      .then(ingrediente => {
        this.formulario.patchValue(ingrediente);
        this.toastr.success('Ingrediente Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.ingrediente = new Ingrediente();
    }.bind(this), 1);
    this.router.navigate(['/ingredientes/cadastrar']);
  }

}
