import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IngredienteService } from './../ingrediente.service';
import { Ingrediente } from 'src/app/model/ingrediente';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationService } from 'primeng';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingrediente-pesquisa',
  templateUrl: './ingrediente-pesquisa.component.html'
})
export class IngredientePesquisaComponent implements OnInit {

  ingredientes: Ingrediente[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private ingredienteService: IngredienteService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.ingredienteService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(resultado => {
      this.ingredientes = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'descricao', header: 'Descrição' },
      { field: 'statusIngrediente', header: 'Status' }
    ];
  }

  alterar(ingrediente) {
    this.router.navigate(['/ingredientes/alterar'], { state: { ingrediente: ingrediente.codigo } });
  }

  confirmarExcluir(ingrediente: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Ingrediente?',
      accept: () => {
        this.excluir(ingrediente);
      }
    });
  }

  excluir(ingrediente: any) {
    this.ingredienteService.excluir(ingrediente.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Ingrediente Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(ingrediente: any): void {
    const novoStatus = !ingrediente.statusIngrediente;

    this.ingredienteService.alterarStatus(ingrediente.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'Ativado' : 'Desativado';
        ingrediente.statusIngrediente = novoStatus;
        this.toastr.success(`Ingrediente ${acao} com Sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
