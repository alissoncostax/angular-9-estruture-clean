import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { PerfilCadastroComponent } from './perfis-cadastro/perfil-cadastro.component';
import { PerfilPesquisaComponent } from './perfis-pesquisa/perfil-pesquisa.component';
import { AuthService } from 'src/app/auth/auth.service';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: PerfilPesquisaComponent,
    canActivate: [AuthGuard],
    data: { profile: [AuthService.PERFIL_ADMIN], roles: ['ROLE_PESQUISAR_PERFIL'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: PerfilCadastroComponent,
    canActivate: [AuthGuard],
    data: { profile: [AuthService.PERFIL_ADMIN], roles: ['ROLE_CADASTRAR_PERFIL'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: PerfilCadastroComponent,
    canActivate: [AuthGuard],
    data: { profile: [AuthService.PERFIL_ADMIN], roles: ['ROLE_ALTERAR_PERFIL'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
