import { NgModule } from '@angular/core';
import { PerfilRoutingModule } from './perfil-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PerfilPesquisaComponent } from './perfis-pesquisa/perfil-pesquisa.component';
import { PerfilCadastroComponent } from './perfis-cadastro/perfil-cadastro.component';
import { TableModule, InputTextModule, MultiSelectModule, DropdownModule, ButtonModule, TooltipModule } from 'primeng';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    PerfilRoutingModule,
    ReactiveFormsModule,

    // Módulo para a Tela
    TableModule,
    InputTextModule,
    MultiSelectModule,
    DropdownModule,
    ButtonModule,
    TooltipModule,
    MultiSelectModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    PerfilCadastroComponent,
    PerfilPesquisaComponent
  ],
  exports: []
})
export class PerfilModule { }
