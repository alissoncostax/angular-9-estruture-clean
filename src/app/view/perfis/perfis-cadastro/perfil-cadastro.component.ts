import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PerfilService } from '../perfil.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Perfil } from 'src/app/model/perfil';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-perfil-cadastro',
  templateUrl: './perfil-cadastro.component.html'
})
export class PerfilCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private perfilService: PerfilService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private breadcrumbService: BreadcrumbService,
  ) {
    this.breadcrumbService.setItems([
      {label: 'Perfil', routerLink: ['/perfis']},
      {label: 'Cadastrar'}
    ]);
  }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoPerfil = history.state.perfil;
    if (codigoPerfil) {
      this.carregarPerfil(codigoPerfil);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoPerfil) {
      this.router.navigate(['/error']);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade, this.validarTamanhoMinimo(5) ] ]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  validarTamanhoMinimo(valor: number) {
    return (input: FormControl) => {
      return (!input.value || input.value.length >= valor) ? null : { tamanhoMinimo: { tamanho: valor } };
    };
  }

  carregarPerfil(codigo: number) {
    this.perfilService.buscarPorCodigo(codigo).then(perfil => {
      this.formulario.patchValue(perfil);
    }).catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarPerfil();
    } else {
      this.adicionarPerfil();
    }
  }

  adicionarPerfil() {
    this.perfilService.adicionar(this.formulario.value).then(perfilAdicionado => {
      this.toastr.success('Perfil Adicionado com Sucesso!');
      this.router.navigate(['/perfis']);
    }).catch(erro => this.errorHandler.handle(erro));
  }

  atualizarPerfil() {
    this.perfilService.atualizar(this.formulario.value).then(perfil => {
      this.formulario.patchValue(perfil);
      this.toastr.success('Perfil Atualizado com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.perfil = new Perfil();
    }.bind(this), 1);
    this.router.navigate(['/perfis/cadastrar']);
  }

}
