import { Component, OnInit } from '@angular/core';
import { PerfilService } from './../perfil.service';
import { Perfil } from 'src/app/model/perfil';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationService } from 'primeng';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil-pesquisa',
  templateUrl: './perfil-pesquisa.component.html'
})
export class PerfilPesquisaComponent implements OnInit {

  perfis: Perfil[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private perfilService: PerfilService,
    private breadcrumbService: BreadcrumbService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) {
    this.breadcrumbService.setItems([
      {label: 'Perfil', routerLink: ['/perfis']},
      {label: 'Listar'}
    ]);
  }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.perfilService.pesquisar().then(resultado => {
      this.perfis = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'status', header: 'Status' }
    ];
  }

  alterar(perfil) {
    this.router.navigate(['/perfis/alterar'], { state: { perfil: perfil.codigo } });
  }

  confirmarExcluir(perfil: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Perfil?',
      accept: () => {
        this.excluir(perfil);
      }
    });
  }

  excluir(perfil: any) {
    this.perfilService.excluir(perfil.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Perfil Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(perfil: any): void {
    const novoStatus = !perfil.status;
    this.perfilService.alterarStatus(perfil.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      perfil.status = novoStatus;
      this.toastr.success(`Perfil ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
