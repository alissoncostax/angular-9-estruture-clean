import { Injectable } from '@angular/core';
import { SisHttp } from 'src/app/auth/sis-http';
import { environment } from 'src/environments/environment';

@Injectable()
export class PermissaoService {

  permissaoUrl: string;

  constructor(private http: SisHttp) {
    this.permissaoUrl = `${environment.apiUrl}/permissoes`;
  }

  listarPermissoes(): Promise<any> {
    return this.http.get(`${this.permissaoUrl}`, { }).toPromise();
  }

}
