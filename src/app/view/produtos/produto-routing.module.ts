import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ProdutoCadastroComponent } from './produtos-cadastro/produto-cadastro.component';
import { ProdutoPesquisaComponent } from './produtos-pesquisa/produto-pesquisa.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ProdutoPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_PRODUTO'] }
  },
  {
    path: 'cadastrar',
    component: ProdutoCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_PRODUTO'] }
  },
  {
    path: ':codigo',
    component: ProdutoCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_PRODUTO'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProdutoRoutingModule { }
