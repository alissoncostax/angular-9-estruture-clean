import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ProdutoRoutingModule } from './produto-routing.module';

import { ProdutoPesquisaComponent } from './produtos-pesquisa/produto-pesquisa.component';
import { ProdutoCadastroComponent } from './produtos-cadastro/produto-cadastro.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectButtonModule, InputMaskModule, MultiSelectModule, DropdownModule, SpinnerModule, RadioButtonModule, ProgressSpinnerModule, ToolbarModule } from 'primeng';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { CurrencyMaskModule } from "ng2-currency-mask";

@NgModule({
  imports: [
    SharedModule,
    ProdutoRoutingModule,
    ReactiveFormsModule,
    
    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    DropdownModule,
    CurrencyMaskModule,
    SpinnerModule,
    RadioButtonModule,
    HttpClientModule,
    FileUploadModule,
    ProgressSpinnerModule,
    ToolbarModule,

  ],
  declarations: [
    ProdutoCadastroComponent,
    ProdutoPesquisaComponent
  ],
  exports: []
})
export class ProdutoModule { }
