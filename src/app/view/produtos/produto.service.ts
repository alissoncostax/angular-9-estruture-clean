import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SisHttp } from 'src/app/auth/sis-http';
import { environment } from 'src/environments/environment';
import { Produto } from 'src/app/model/produto';

@Injectable()
export class ProdutoService {

  produtoUrl: string;

  constructor(private http: SisHttp) {
    this.produtoUrl = `${environment.apiUrl}/produtos`;
  }

  pesquisar(empresa: number): Promise<any> {
    return this.http.get<any>(`${this.produtoUrl}/empresa/${empresa}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.produtoUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(produto: Produto): Promise<Produto> {
    return this.http.post<Produto>(this.produtoUrl, produto).toPromise();
  }

  // Método de Atualizar
  atualizar(produto: Produto): Promise<Produto> {
    return this.http.put<Produto>(`${this.produtoUrl}/${produto.codigo}`, produto).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.produtoUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<Produto> {
    return this.http.get<Produto>(`${this.produtoUrl}/${codigo}`).toPromise();
  }

  urlUploadImagem(): string {
    return `${this.produtoUrl}/imagem`
  }

}
