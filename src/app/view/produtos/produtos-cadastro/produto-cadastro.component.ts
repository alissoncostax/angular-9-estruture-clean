import { IngredienteService } from './../../ingredientes/ingrediente.service';
import { SelectItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

import { ProdutoService } from './../produto.service';
import { Ingrediente } from 'src/app/model/ingrediente';
import { CategoriaService } from '../../categorias/categoria.service';
import { UnidadeMedidaService } from '../../unidadesmedida/unidademedida.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Produto } from 'src/app/model/produto';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-produto-cadastro',
  templateUrl: './produto-cadastro.component.html'
})
export class ProdutoCadastroComponent implements OnInit {

  formulario: FormGroup;

  unidadesMedida: SelectItem[];
  categorias: SelectItem[];
  ingredientesBase: Ingrediente[];
  ingredientes: Ingrediente[];

  uploadEmAndamento: Boolean = false;

  httpHeaders: HttpHeaders = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));

  constructor(
    private auth: AuthService,
    private produtoService: ProdutoService,
    private ingredienteService: IngredienteService,
    private categoriaService: CategoriaService,
    private unidadeMedidaService: UnidadeMedidaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    this.listarIngredientes();

    this.listarCategorias();

    this.listarUnidadesMedida();

    const codigoProduto = history.state.produto;
    if (codigoProduto) {
      this.carregarProduto(codigoProduto);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoProduto) {
      this.router.navigate(['/error']);
    }

  }

  // Momento em que o Upload será Realizado
  antesUploadImagem() {
    this.uploadEmAndamento = true;
  }

  aoTerminarUploadImagem(retornoS3) {
    this.formulario.patchValue({
      imagem: retornoS3.originalEvent.body.nome,
      urlImagem: retornoS3.originalEvent.body.url
    });
    this.uploadEmAndamento = false;
  }

  erroUpload(event) {
    this.toastr.error('Erro ao Tentar enviar Imagem do Produto.');
    this.uploadEmAndamento = false;
  }

  removerImagem() {
    this.formulario.patchValue({
      imagem: null,
      urlImagem: null
    });
  }

  get nomeAnexo() {
    const nome = this.formulario.get('imagem').value;
    if (nome) {
      return nome.substring(nome.indexOf('_') + 1, nome.length);
    }
    return '';
  }

  get urlUploadImagem() {
    return this.produtoService.urlUploadImagem();
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: null,
      numIdentificador: ['', [ this.validarObrigatoriedade ]],
      descricao: ['', [ this.validarObrigatoriedade ]],
      ingredientes: null,
      categoria: this.formBuilder.group({
        codigo: [null, [ this.validarObrigatoriedade ]]
      }),
      unidadeMedida: this.formBuilder.group({
        codigo: null
      }),
      quantidadeMedida: null,
      valor: ['', [ this.validarObrigatoriedade ]],
      imagem: [],
      urlImagem: [],
      tipoComplemento: [],
      empresa: [this.auth.jwtPayLoad.codigoEmpresa]
    });
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  listarCategorias() {
    this.categoriaService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(categoria => {
      const categoriasBanco = categoria;
      this.categorias = categoriasBanco.map(p =>
        ({ label: p.descricao, value: p.codigo })
      );
      this.categorias.splice(0, 0, {label: 'Selecione uma Categoria', value: null});
    }).catch(erro => this.errorHandler.handle(erro));
  }

  listarUnidadesMedida() {
    this.unidadeMedidaService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa)
      .then(unidadesMedida => {
        const unidadesMedidaBanco = unidadesMedida;
        this.unidadesMedida = unidadesMedidaBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
        this.unidadesMedida.splice(0, 0, {label: 'Selecione uma Unidade de Medida', value: null});
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  listarIngredientes() {
    this.ingredienteService.listarIngredientes(this.auth.jwtPayLoad.codigoEmpresa)
      .then(ingredientes => {
        const ingredienteBanco = ingredientes.ingrediente;
        this.ingredientes = ingredienteBanco;
        this.ingredientesBase = ingredienteBanco;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarProduto(codigo: number) {
    this.produtoService.buscarPorCodigo(codigo)
      .then(produto => {
        this.formulario.patchValue(produto);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarProduto();
    } else {
      this.adicionarProduto();
    }
  }

  adicionarProduto() {
    this.produtoService.adicionar(this.formulario.value)
      .then(produtoAdicionado => {
        this.toastr.success('Produto Adicionado com Sucesso!');
        this.router.navigate(['/produtos']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarProduto() {
    this.produtoService.atualizar(this.formulario.value)
      .then(produto => {
        this.formulario.patchValue(produto);
        this.toastr.success('Produto Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.produto = new Produto();
    }.bind(this), 1);
    this.router.navigate(['/produtos/cadastrar']);
  }

}
