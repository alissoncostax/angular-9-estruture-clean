import { Component, OnInit } from '@angular/core';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { ProdutoService } from './../produto.service';
import { Produto } from 'src/app/model/produto';
import { AuthService } from 'src/app/auth/auth.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produto-pesquisa',
  templateUrl: './produto-pesquisa.component.html'
})
export class ProdutoPesquisaComponent implements OnInit {

  categorias: SelectItem[];

  produtos: Produto[];
  cols: any[];

  constructor(
    // Não Remover, pois a tela Utiliza para Validar os Botões
    private auth: AuthService,
    private produtoService: ProdutoService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.produtoService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(resultado => {
      this.produtos = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'numIdentificador', header: 'Código', width: '10%' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'categoria', header: 'Categoria', width: '20%' },
      { field: 'valor', header: 'Valor', width: '15%' },
      { field: 'statusProduto', header: 'Status', width: '10%' }
    ];
  }

  alterar(produto) {
    this.router.navigate(['/produtos/alterar'], { state: { produto: produto.codigo } });
  }

  confirmarExcluir(produto: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Produto?',
      accept: () => {
        this.excluir(produto);
      }
    });
  }

  excluir(produto: any) {
    this.produtoService.excluir(produto.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Produto Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(produto: any): void {
    const novoStatus = !produto.statusProduto;

    this.produtoService.alterarStatus(produto.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      produto.statusProduto = novoStatus;
      this.toastr.success(`Produto ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
