import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
})
export class AppLoginComponent implements OnInit {

  formulario: FormGroup;

  // No Construtor é necessário Injetar o Serviço para Usar
  constructor(
    private auth: AuthService, 
    private errorHandler: ErrorHandlerService, 
    private router: Router,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      email: [ '', [ this.validarObrigatoriedade, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$') ] ],
      senha: [ '', [ this.validarObrigatoriedade ] ]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  login() {
    this.auth.login(this.formulario.get('email').value, this.formulario.get('senha').value).then(() => {
      this.router.navigate(['/']);
    }).catch(erro => {
      this.errorHandler.handle(erro);
    });
  }

}
