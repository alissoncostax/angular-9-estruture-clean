import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { SisHttp } from '../../auth/sis-http';
import { environment } from 'src/environments/environment';

@Injectable()
export class LogoutService {

  tokenRevokeUrl: string;

  constructor(private http: SisHttp, private auth: AuthService) {
    this.tokenRevokeUrl = `${environment.apiUrl}/tokens/revoke`;
  }

  logout() {
    return this.http.delete(this.tokenRevokeUrl, { withCredentials: true }).toPromise().then(() => {
      this.auth.limparAcessToken();
    });
  }

}
