/**
 * Serviço Auth de Http, onde será Fornecido para Toda a Autenticação,
 * pois está sendo Importando pelo app.module.js
 */
import { NgModule } from '@angular/core';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuard } from '../../auth/auth.guard';
import { SegurancaRoutingModule } from './seguranca-routing.module';
import { LogoutService } from './logout.service';
import { ButtonModule, InputTextModule } from 'primeng';
import { AppLoginComponent } from './login-form/app.login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { environment } from 'src/environments/environment';
import { ReactiveFormsModule } from '@angular/forms';

// Retorna o Token para a Confiuração do JwtModule do JWT Auth0
export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  imports: [
    SegurancaRoutingModule,
    SharedModule,
    ReactiveFormsModule,

    // Módulos que serão Trabalhados pela Página de Login/Segurança
    ButtonModule,
    InputTextModule

  ],
  declarations: [AppLoginComponent],
  providers: [
    AuthGuard,
    LogoutService
  ]
})
export class SegurancaModule { }
