import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { UnidadeMedidaCadastroComponent } from './unidadesmedida-cadastro/unidademedida-cadastro.component';
import { UnidadeMedidaPesquisaComponent } from './unidadesmedida-pesquisa/unidademedida-pesquisa.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: UnidadeMedidaPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_UNIDADE_MEDIDA'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: UnidadeMedidaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_UNIDADE_MEDIDA'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: UnidadeMedidaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_UNIDADE_MEDIDA'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UnidadeMedidaRoutingModule { }
