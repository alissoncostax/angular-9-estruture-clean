import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { UnidadeMedidaRoutingModule } from './unidademedida-routing.module';
import { UnidadeMedidaCadastroComponent } from './unidadesmedida-cadastro/unidademedida-cadastro.component';
import { UnidadeMedidaPesquisaComponent } from './unidadesmedida-pesquisa/unidademedida-pesquisa.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    UnidadeMedidaRoutingModule,
    ReactiveFormsModule,
    
    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule, 
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    UnidadeMedidaCadastroComponent,
    UnidadeMedidaPesquisaComponent
  ],
  exports: []
})
export class UnidadeMedidaModule { }
