import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SisHttp } from 'src/app/auth/sis-http';
import { environment } from 'src/environments/environment';
import { UnidadeMedida } from 'src/app/model/unidademedida';

@Injectable()
export class UnidadeMedidaService {

  unidadeMedidaUrl: string;

  constructor(private http: SisHttp) {
    this.unidadeMedidaUrl = `${environment.apiUrl}/unidadesmedida`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(empresa: number): Promise<any> {
    return this.http.get<any>(`${this.unidadeMedidaUrl}/empresa/${empresa}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.unidadeMedidaUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(unidadeMedida: UnidadeMedida): Promise<UnidadeMedida> {
    return this.http.post<UnidadeMedida>(this.unidadeMedidaUrl, unidadeMedida).toPromise();
  }

  // Método de Atualizar
  atualizar(unidadeMedida: UnidadeMedida): Promise<UnidadeMedida> {
    return this.http.put<UnidadeMedida>(`${this.unidadeMedidaUrl}/${unidadeMedida.codigo}`, unidadeMedida).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.unidadeMedidaUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  // Carregar a Combo com Todos os Perfis ATIVOS
  listarPerfis(): Promise<any> {
    // Filtrar por Status Ativo
    const params = new HttpParams().append('status', 'true');
    return this.http.get(`${this.unidadeMedidaUrl}`, { params }).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<UnidadeMedida> {
    return this.http.get<UnidadeMedida>(`${this.unidadeMedidaUrl}/${codigo}`).toPromise();
  }

}
