import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { UnidadeMedidaService } from './../unidademedida.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UnidadeMedida } from 'src/app/model/unidademedida';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-unidademedida-cadastro',
  templateUrl: './unidademedida-cadastro.component.html'
})
export class UnidadeMedidaCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private auth: AuthService,
    private unidadeMedidaService: UnidadeMedidaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoUnidadeMedida = history.state.unidadeMedida;
    if (codigoUnidadeMedida) {
      this.carregarUnidadeMedida(codigoUnidadeMedida);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoUnidadeMedida) {
      this.router.navigate(['/error']);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade ] ],
      sigla: [ '', [ this.validarObrigatoriedade ] ],
      empresa: [this.auth.jwtPayLoad.codigoEmpresa]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarUnidadeMedida(codigo: number) {
    this.unidadeMedidaService.buscarPorCodigo(codigo)
      .then(unidadeMedida => {
        this.formulario.patchValue(unidadeMedida);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarUnidadeMedida();
    } else {
      this.adicionarUnidadeMedida();
    }
  }

  adicionarUnidadeMedida() {
    this.unidadeMedidaService.adicionar(this.formulario.value)
      .then(unidadeMedidaAdicionado => {
        this.toastr.success('Unidade de Medida Adicionada com Sucesso!');
        this.router.navigate(['/unidadesmedida']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarUnidadeMedida() {
    this.unidadeMedidaService.atualizar(this.formulario.value)
      .then(unidadeMedida => {
        this.formulario.patchValue(unidadeMedida);
        this.toastr.success('Unidade de Medida Atualizada com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.unidadeMedida = new UnidadeMedida();
    }.bind(this), 1);
    this.router.navigate(['/unidadesmedida/cadastrar']);
  }

}
