import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UnidadeMedidaService } from './../unidademedida.service';
import { UnidadeMedida } from 'src/app/model/unidademedida';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationService } from 'primeng';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unidademedida-pesquisa',
  templateUrl: './unidademedida-pesquisa.component.html'
})
export class UnidadeMedidaPesquisaComponent implements OnInit {

  unidadesmedida: UnidadeMedida[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private unidadeMedidaService: UnidadeMedidaService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.unidadeMedidaService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(resultado => {
        this.unidadesmedida = resultado
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'sigla', header: 'Sigla' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'status', header: 'Status' }
    ];
  }

  alterar(unidadeMedida) {
    this.router.navigate(['/unidadesmedida/alterar'], { state: { unidadeMedida: unidadeMedida.codigo } });
  }

  confirmarExcluir(unidadeMedida: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Unidade de Medida?',
      accept: () => {
        this.excluir(unidadeMedida);
      }
    });
  }

  excluir(unidadeMedida: any) {
    this.unidadeMedidaService.excluir(unidadeMedida.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Unidade de Medida Excluída com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(unidadeMedida: any): void {
    const novoStatus = !unidadeMedida.status;

    this.unidadeMedidaService.alterarStatus(unidadeMedida.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativada' : 'Desativada';
      unidadeMedida.status = novoStatus;
      this.toastr.success(`Unidade de Medida ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
