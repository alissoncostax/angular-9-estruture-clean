import { UsuarioRoutingModule } from './usuario-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsuarioPesquisaComponent } from './usuarios-pesquisa/usuario-pesquisa.component';
import { UsuarioCadastroComponent } from './usuarios-cadastro/usuario-cadastro.component';
import { TableModule, TooltipModule, ButtonModule, InputTextModule, DropdownModule, PasswordModule } from 'primeng';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    UsuarioRoutingModule,
    ReactiveFormsModule,

    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    PasswordModule
  ],
  declarations: [
    UsuarioCadastroComponent,
    UsuarioPesquisaComponent
  ],
  exports: []
})
export class UsuarioModule { }
