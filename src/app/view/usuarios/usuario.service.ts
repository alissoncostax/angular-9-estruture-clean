import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SisHttp } from 'src/app/auth/sis-http';
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/app/model/usuario';

@Injectable()
export class UsuarioService {

  usuarioUrl: string;

  constructor(private http: SisHttp) {
    this.usuarioUrl = `${environment.apiUrl}/usuarios`;
  }

  /**
   * POST
   */
  adicionar(usuario: Usuario): Promise<Usuario> {
    return this.http.post<Usuario>(this.usuarioUrl,  usuario).toPromise();
  }

  /**
   * GET
   */
  pesquisar(empresa: number): Promise<any> {
    return this.http.get<any>(`${this.usuarioUrl}/empresa/${empresa}`).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Usuario> {
    return this.http.get<Usuario>(`${this.usuarioUrl}/${codigo}`).toPromise();
  }

  /**
   * PUT
   */
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.usuarioUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  atualizar(usuario: Usuario): Promise<Usuario> {
    return this.http.put<Usuario>(`${this.usuarioUrl}/${usuario.codigo}`, usuario).toPromise();
  }

  /**
   * DELETE
   */
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.usuarioUrl}/${codigo}`).toPromise();
  }

}
