import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { Usuario } from 'src/app/model/usuario';
import { UsuarioService } from './../usuario.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/auth/auth.service';

// Decorador de Utilização desse Componente
@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html'
})
export class UsuarioCadastroComponent implements OnInit {

  formulario: FormGroup;

  /**
   * Injeção de Dependência, o Construtor é o Primeiro método a ser Carregado
   * ao Instanciar a Classe
   */
  constructor(
    private auth: AuthService,
    // Apresenta os Erros
    private errorHandler: ErrorHandlerService,
    // Responsável pelos Serviços que o Usuário fornece
    private usuarioService: UsuarioService,
    // Apresenta mensagens na Tela
    private toastr: ToastrService,
    // Através dele é possivel verificar qual a Rota Sendo Chamada
    private route: ActivatedRoute,
    // Possibilita a Navegação Imperativa, Levando para outras Páginas
    private router: Router,
    // Responsável pela Configuração do Formulário Reativo
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();
    
    const codigoUsuario = history.state.usuario;
    if (codigoUsuario) {
      this.carregarUsuario(codigoUsuario);
    } else if (this.route.snapshot.routeConfig.path === 'alterar' && !codigoUsuario) {
      this.router.navigate(['/error']);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      nome: [ '', [ this.validarObrigatoriedade ] ],
      login: [ '', [ this.validarObrigatoriedade ] ],
      senha: [ '', [ this.validarObrigatoriedade ] ],
      confirmarSenha: [ '', [ this.validarObrigatoriedade ] ],
      empresa: [this.auth.jwtPayLoad.codigoEmpresa]
    });
  }

  adicionarUsuario() {
    this.usuarioService.adicionar(this.formulario.value).then(usuarioAdicionado => {
      this.toastr.success('Usuário Adicionado com Sucesso!');
      this.router.navigate(['/usuarios']);
    }).catch(erro => this.errorHandler.handle(erro));
  }

  atualizarUsuario() {
    this.usuarioService.atualizar(this.formulario.value)
      .then(usuario => {
        this.formulario.patchValue(usuario);
        this.toastr.success('Usuário Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarUsuario(codigo: number) {
    this.usuarioService.buscarPorCodigo(codigo).then(usuario => {
      this.formulario.patchValue(usuario);
    }).catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    // POG para permitir o Tempo de Carregamento do Form
    setTimeout(function() {
      this.usuario = new Usuario();
    }.bind(this), 1);
    this.router.navigate(['/usuarios/novo']);
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarUsuario();
    } else {
      this.adicionarUsuario();
    }
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    if (!input.root.get('codigo') || input.root.get('codigo').value === '') {
      return (input.value ? null : { obrigatoriedade: true });
    } else {
      if (input.root.get('senha') || input.root.get('confirmarSenha')) {
        if (input.root.get('senha').value) {
          return (input.value ? null : { obrigatoriedade: true });
        }
        return null;
      }
    }
  }

  validarCombinacaoSenha(input: FormControl) {
    if (!input.root.get('codigo') || input.root.get('codigo').value === '') {
      const senha = input.root.get('senha');
      const confirmarSenha = input.root.get('confirmarSenha');

      if (senha && confirmarSenha) {
        return (senha.value === confirmarSenha.value ? null : { senhasNaoCoincidem: true });
      }
      return null;
    } else {
      return null;
    }
  }

}
