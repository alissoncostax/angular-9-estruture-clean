import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './../usuario.service';
import { Usuario } from 'src/app/model/usuario';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationService, MessageService } from 'primeng';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuario-pesquisa',
  templateUrl: './usuario-pesquisa.component.html'
})
export class UsuarioPesquisaComponent implements OnInit {

  usuarios: Usuario[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private usuarioService: UsuarioService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.usuarioService.pesquisar(this.auth.jwtPayLoad.codigoEmpresa).then(resultado => {
      this.usuarios = resultado
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'nome', header: 'Nome' },
      { field: 'login', header: 'Login' },
      { field: 'perfil', header: 'Perfil', width: '15%'},
      { field: 'status', header: 'Status', width: '10%' }
    ];
  }

  alterar(usuario) {
    this.router.navigate(['/usuarios/alterar'], { state: { usuario: usuario.codigo } });
  }

  confirmarExcluir(usuario: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Usuário?',
      accept: () => {
        this.excluir(usuario);
      }
    });
  }

  excluir(usuario: any) {
    this.usuarioService.excluir(usuario.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Usuário Excluído com Sucesso!');
    })
    .catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(usuario: any): void {
    const novoStatus = !usuario.status;
    this.usuarioService.alterarStatus(usuario.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      usuario.status = novoStatus;
      this.toastr.success(`Usuário ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
