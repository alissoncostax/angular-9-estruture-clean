/**
 * environment de Produção
 */
export const environment = {
  production: true,
  apiUrl: 'https://menudigital-api.herokuapp.com',

  tokenWhitelistedDomains: [ /menudigital-api.herokuapp.com/ ],
  tokenBlacklistedDomains: [ /\/oauth\/token/ ]
};
